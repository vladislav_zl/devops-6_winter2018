#!/bin/bash
# devops-6_winter2018/tasks/bash_linux/exercise0.md

function config {
# Function "config" (extended version):
# - the number and order of the arguments can be random;
# - at least one parameter must be specified, it is a template file with "*.tpl" extension 
#   and this file have to exist in current folder. Otherwise you need specify or full either relative path;
# - if the name of resulting file is not specified, the "result.conf" file will be created by default;
#
# You may specify as argument of script any global envinroment variable if the corresponding parameter is defined in template file. 
# For this you need use the variable name in low case (e.g. for "$PWD" specify argument "pwd").
#
# At the same time, you can create your own configuration parameters in template file and define their values for current session.
# For example, add string "SOME_PARAMETER={some_parameter}" to template file, then 
# define it for current bash session (e.g. "user@ubuntu:~/work_dir$ export SOME_PARAMETER=12345")
# and pick word some_parameter" as on of arguments for the script. 

# default settings
template_file=""
result_file="result.conf"
i=0
array=()

# RegExp
regex1="[^.]+\.[^.]+$"
regex2="[^.]+\.tpl$"

#echo "INPUT PARAMS is: $@"

# Parameters processing
for param in "$@"
do 
   if [[ $param =~ $regex1 ]]
   then 
      if [[ $param =~ $regex2 ]]
      then 
         [ -f $param ] 
         if [ $? -ne 0 ]
         then
            echo "File $1 doesn't exist!"
            return  
         else 
            template_file=$param
         fi
      else
         result_file=$param
      fi
   else
#echo "array[$i] = $param" 
      array[$i]=$param
      i=$(($i + 1))
   fi
done 

if [[ $template_file = "" ]] 
then
   echo "Template file with '.tpl' extension must be specified!"
   return
fi

# Creating a conf file based on the template

temporary_file="temp_file.conf"
cp $template_file $temporary_file

for param in ${array[@]}
do
  var=${param^^}
  eval var=\$$var
  sed -e 's!={'$param'}!='$var'!' $temporary_file > $result_file
  cp $result_file $temporary_file
done

# Replace all absent parameters to empty
sed -e 's!={.*}!=!' $temporary_file > $result_file
#
# end of processing
#
echo -e "Congratulations to you, the config file \"$result_file\" was successful created!\n"
cat $result_file
rm $temporary_file
return 
}

# Call function "config" 
# 
result=$(config $@)
echo $result
