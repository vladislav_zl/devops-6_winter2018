#!/usr/bin/env bash
. ./script2.conf

# main task
for FILE in "${INPUT_DIR}"/*; do
    INPUT_FILE=$(basename "${FILE}")
    # don't process lock files
    if [[ "${FILE}" =~ \.lock$ ]]; then
        continue
    fi
    # if file already moved, skip processing
    if [ ! -f "${FILE}" ];then
        continue
    fi
    # if lock file exist, skip this file
    if [ -f "${FILE}".lock ]; then
        continue
    else
        # make lock file and check it
        cp /dev/null "${FILE}.lock"
        if [ $? -eq 1 ]; then
            continue
        fi
        # check last string of file for UUID
        LAST_STR=$(tail -n 1 "${FILE}")
        if [[ ! "${LAST_STR}" =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
            rm "${FILE}.lock"
            continue
        fi
        # move and rename file
        FIRST_STR=$(head -n 1 "${FILE}")
        FILE_NAME=$(echo "${FIRST_STR}" | cut -f 1)
        FILE_DATE=$(echo "${FIRST_STR}" | cut -f 2)
        FILE_LAST_MOD=$(date --date="$(stat -c %y "${FILE}")" +"%Y%m%d-%H%M%S")
        OUT_FILE_NAME_TEMP="${FILE_NAME}_${FILE_DATE}_${FILE_LAST_MOD}"
	OUT_FILE_NAME=$(echo "${OUT_FILE_NAME_TEMP// /.}")
        mv "${FILE}" "${OUTPUT_DIR}/${OUT_FILE_NAME}"
        # write to log
        TIME_LOG=$(date +"%Y-%m-%d %H:%M:%S")
        echo "${TIME_LOG} File ${FILE} moved to ${OUTPUT_DIR}/${OUT_FILE_NAME}" >> "${LOG_FILE}"
        # remove lock file
        rm "${FILE}.lock"
    fi
done

# report part
# if there are many instances of script, only last will generate report
COUNT_INSTANCES=$(pgrep -f -c $(basename $0))
if (( COUNT_INSTANCES > "1" )); then
    exit 1
fi
cp /dev/null report_temp_file
if [ $? -eq 1 ]; then
    exit 2
fi
# write all appropriate objects to temp file
for FILE in "${OUTPUT_DIR}"/*; do
    OUTPUT_FILE=$(basename "${FILE}")
    OUTPUT_FILE_DATE=$(echo "${OUTPUT_FILE}" | awk -F "_" '{print $2}' | awk -F "." 'BEGIN { OFS = ""} {print $3,$2,$1}')
    if [[ "${OUTPUT_FILE_DATE}" > "${REPORT_BEGIN_DATE}" && "${OUTPUT_FILE_DATE}" < "${REPORT_END_DATE}" ]]; then
        OUTPUT_FILE_NAME=$(echo "${OUTPUT_FILE}" | awk -F "_" '{print $1}')
        echo "${OUTPUT_FILE_NAME}" >> report_temp_file
    fi
done
# awk process temp file to report file
echo "Report for period ${REPORT_BEGIN_DATE} to ${REPORT_END_DATE}" > "${REPORT_FILE}"
cat report_temp_file | awk -f report.awk >> "${REPORT_FILE}"
rm  report_temp_file
